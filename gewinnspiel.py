import random

def contest():
    with open("teilnehmer.txt", "r") as file:
        contestmember = []
        tmpnumb = 1
        for line in file.readlines():
            if line[-1:] == "\n":
                contestmember.append(line[:-1])
            else:
                contestmember.append(line)
        numbofwins = input("Number of winners?")
        for i in range (0, int(numbofwins)):
            rn = random.randint(0, len(contestmember) - 1)
            print(contestmember[rn])
            del contestmember[rn]


if __name__ == '__main__':
    contest()
